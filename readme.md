
<!--
Copyright 2017-2018 Hangar of the Future - Airbus Singapore Pte Ltd

This file is part of front-end solution for Smart Mobility.

Smart Mobility is a property of Hangar of the Future - Airbus Singapore Pte Ltd  .
Any distribution or modifications must be made aware to the owner of the source code.
-->
# SMART MOBILITY APPLICATION

Smart Mobility  is web-based application developed to help make performing maintenance task more efficient.
The application provides *single-point* for information such as **digital task card**, **electronic manual** and **tool information**.
It is also enhanced with features to **remote call to expert**, **take/edit/annotate picture**, etc.

Libraries:
* [AngularJS 1.6.6](https://angularjs.org/)
* [Bootstrap Version 3.3.7](https://getbootstrap.com/docs/3.3/getting-started/)
* [UI bootstrap](https://angular-ui.github.io/bootstrap/)
* [Webcam JS](https://github.com/jhuckaby/webcamjs)
* [Socket.io](https://socket.io/)
* [AngularJS Notification](https://github.com/alexcrack/angular-ui-notification)

## ARCHITECTURE

Refer to HaoFu_SmartMob_Implementation_v1.3.pptx

## CREATING ANGULARJS APPLICATION

1. Add the AngularJS library in HTML (usually in `index.html`, preferably above the `</body>`)

```html
<!-- AngularJS -->
<!-- attempt to load AngularJS from CDN -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js">
</script>
<!-- if AngularJS fails to load from CDN, fallback a load local version -->
<script>window.angular || document.write('<script src="assets/js/angular.min.js"><\/script>');</script>
```

2. Create AngularJS module in file `app/app.module.js`

```javascript
// Define the angular app and include all the required libraries
angular
.module('mmobApp', ['ui.router', 'ui.bootstrap', 'webcam', 'ngCookies', 'ngAnimate', 'ui-notification'])
```

3. Bind the `<body>` to the AngularJS app using `ng-app`

```html
  ...

  <body ng-app='mmobApp'>
  </body>
```

## FOLDER STRUCTURE

Follow the Folders-by-Feature structure as proposed by [John Papa's guide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md#application-structure).
Our application folder structure is as follow:

```
/* app will contain all the angular components for the MMob application */
app/
  app.config.js
  app.module.js
  components/
    dashboard/
      dashboard.view.html
      jobcard.controller.js
      jobcard.view.html
    login/
      login.controller.js
      login.view.html
  shared/
    indexDB/
    socket/
assets/
  css/
  fonts/
  images/
vendor/
index.html
```
## ENVIRONMENT CONFIGURATION

Environment configuration is stored in `app/env.js`. <br />
The following configuration should be modified accordingly:

```javascript
// location of the tool booking refer to Architecture document for the URL
window.__env.bookedUrl = 'http://192.168.100.217'

// location of the Smart Mobility API refer to Architecture document for the URL
window.__env.apiUrl = 'http://192.168.100.217:3000';
window.__env.baseUrl = '/api/smartmob'; // base URL after the domain name

// Setting to enable console.log(), true - enable, false - disable (console.log() prints nothing)
window.__env.enableDebug = true;
```

## SOFTWARE DESIGN PATTERN

Smart Mobility uses the Model-View-Controller design pattern as much as it allows.
Each feature in Smart Mobility application has `*.view.html` (View), `*.controller.js` (Controller) and most times `*.service.js` (Model).

#### VIEW

All the `*.view.html` files contains the HTML, CSS and AngularJS brace notation `{{variable}}` to bind expressions to elements.

Example:
```html
<table ng-controller='ToolController as ToolCtrl'>
  <tbody>
    <tr ng-repeat='tool in ToolCtrl.toolList'>
      <td>
        <h3>{{tool.name}}</h3>
      </td>
      <td class='text-center'>
        <h3>{{tool.partNo}}</h3>
      </td>
    </tr>
  </tbody>
</table>
```

In the example above, we are displaying a list of tools retrieved using the controller, `ToolController`.

#### CONTROLLER

To achieve consistency, defining controller in Smart Mobility is done as follows:
```javascript
(function() {
  'use strict';

  // Register the controller to the module
  angular
    .module('mmobApp')
    .controller('TaskController', TaskController);

  // Inject the required dependencies
  TaskController.$inject = ['$scope', '$state', '$http', '$stateParams', '$uibModal', 'TaskService', 'SocketService', 'Notification', '$q', '$window', '$cookieStore', 'Base64'];

  // Define the controller constructor with the dependencies
  function TaskController($scope, $state, $http, $stateParams, $uibModal, TaskService, SocketService, Notification, $q, $window, $cookieStore, Base64) {

    // use vm variables
    var vm = this;

    // Functions in the Controllers
    vm.FindTask = function(taskId) {

      // Function implementations
      ...
    }

  }

}();
```

Controllers can be attached to the DOM element using `ng-controller` directive.<br />
It is recommended to use the `controller as` syntax when binding to DOM element as follows:

```html
<div class='login-page' ng-controller='LoginController as loginCtrl'>
	<button id='login-button' type='submit' class='btn btn-primary' ng-click='loginCtrl.Authenticate()'>Login</button>
</div>
```

Controller should be used to handle business logic and leave the handling of data model to Service.

#### SERVICE

Service in Smart Mobility are mostly used to consume the API from the back-end.
Controller will call a service function and receive data from the service.

```javascript
(function() {
  'use strict';

  // Register the service to the module
  angular
    .module('mmobApp')
    .factory('TaskService', TaskService);

  TaskService.$inject = ['$http', '$q', '__env'];

  function TaskService($http, $q, __env) {

  // Inject the required dependencies
  TaskService.$inject = ['$scope', '$state', '$http', '$stateParams', '$uibModal', 'TaskService', 'SocketService', 'Notification', '$q', '$window', '$cookieStore', 'Base64'];

  // Define the controller constructor with the dependencies
  function TaskService($scope, $state, $http, $stateParams, $uibModal, TaskService, SocketService, Notification, $q, $window, $cookieStore, Base64) {

    // Define the service functions
    var service = {
      ListByJobcardID : ListByJobcardID,
      UpdateByID : UpdateByID
    }

    return service;

    function ListByJobcardID(jobcardID) {
      // implementations

    }

    function UpdateByID(task) {
      // implementations

    }
  }

}();
```

To use the service, inject the service into the controller as necessary e.g:

```javascript
  ...

  // Inject the required dependencies
  TaskController.$inject = ['TaskService'];

  // Define the controller constructor with the dependencies
  function TaskController(TaskService) {
    ...
  }
```

## ROUTING MECHANISM

Smart Mobility application uses UI-Routing library found in https://ui-router.github.io/.

```javascript
angular
  .module('mmobApp')
  .config(config);

  function config($stateProvider, $urlRouterProvider) {

    // Default child to display when going to dashboard view
    $urlRouterProvider.when('/dashboard', '/dashboard/jobcard');
    $urlRouterProvider.otherwise('/login');

    // Create the states
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/components/login/login.view.html'
      })
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'app/components/dashboard/dashboard.view.html'
      })
      .state('jobcard', {
        url: '/jobcard',
        parent: 'dashboard', // set the jobcard to display within dashboard
        templateUrl: 'app/components/dashboard/jobcard.view.html'
      })
  };
```

To go to another state using JavaScript code:

```javascript

  $state.go('dashboard');

```

# Camera Functionality using indexedDB

The application includes picture-taking functionality.<br />
Pictures can be taken from the application using web camera attached to the device. <br />
IndexedDB feature, which is supported by almost all the web browser, is used to cache the pictures.<br />

Refer to [Working with IndexedDB](https://developers.google.com/web/ilt/pwa/working-with-indexeddb) <br />

```indexeddb.service.js``` is a simple AngularJS service to handle this feature.<br />

The application uses this service as follows:
1. Open and specify the db name as first parameter and storage name as second parameter
```javascript
IndexedDBService.Open('mmobDB', 'photos').then(function() {
  console.log('mmobDB is opened successfully');
});
```

2. Add data
```javascript
// first parameter is storage name
// second parameter is data
// third parameter is key for the data
IndexedDBService.Add('photos', data, photo['photoid']).then(function() {
  console.log('Added new photo successfully');
});
```

# Commonly encountered problems

#### ```[$injector:XXXXX]``` or ```[$controller:ctrlreg]``` error
It is usually caused by not including the js file in to the index.html. <br />
Please ensure that the js file(s) is included.

#### Problems during minification
During minifcation, you need to ensure the parameters is injected properly
Example of filter:
```
angular
.module('mmobApp')
// Ensure the $filter parameter is injected
.filter('hideIfEmpty', ['$filter', function($filter) {
  return function (dateString, format) {
    // console.log(dateString);
    if(dateString === '0000-00-00 00:00:00') {
        return "";
    } else {
        return $filter('date')(dateString, format.toString());
    }
  };
}]);
```

Other example:
```
angular
  .module('mmobApp')
  .factory('SocketService', SocketService);

// Must ensure you inject the parameters accordingly
SocketService.$inject = ['$rootScope', '__env'];

function SocketService($rootScope, __env) {
  var socket = io(__env.apiUrl);

  var service = {
    On: Listen,
    Emit: Send
  };

  ...
}
```

# REFERENCES

* [Good Practices to Build Your AngularJS Application](https://www.thoughtworks.com/insights/blog/good-practices-build-your-angularjs-application)
* [Introduction to the AngularJS by example tutorial series](https://blog.revillweb.com/introduction-to-the-angularjs-by-example-tutorial-series-38409be62629)
