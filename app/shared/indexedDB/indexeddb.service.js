/************************************************************/
/* Purpose: This is a service for the indexeddb functionalities.
            To use this service, inject the service in controller.
/************************************************************/

(function (){
  'use Strict'

  angular
  .module('mmobApp')
  .factory('IndexedDBService', IndexDBService);

  IndexDBService.$inject = ['$window', '$q'];

  function IndexDBService($window, $q) {

    var indexedDB = $window.indexedDB;
    var db = null;
    var lastIndex = 0;

    /* Call this function IndexedDBService.Open(), provide database and store name */
    var open = function (dbName, storeName) {
        var deferred = $q.defer();
        var version = 12;
        var request = indexedDB.open(dbName, version);

        request.onupgradeneeded = function (e) {
            db = e.target.result;

            e.target.transaction.onerror = indexedDB.onerror;

            if (db.objectStoreNames.contains(storeName)) {
              console.log('deleting : '+storeName);
              db.deleteObjectStore(storeName);
            }

            var store = db.createObjectStore(storeName, {
                autoIncrement: false
            });
        };

        request.onsuccess = function (e) {
            db = e.target.result;

            deferred.resolve();
        };

        request.onerror = function () {
            deferred.reject();
        };

        return deferred.promise;
    };

    var get = function (storeName) {
        var deferred = $q.defer();

        if (db === null) {
            deferred.reject('IndexDB is not opened yet!');
        } else {
            var trans = db.transaction([storeName], 'readwrite');
            var store = trans.objectStore(storeName);
            var todos = [];

            // Get everything in the store;
            var keyRange = IDBKeyRange.lowerBound(0);
            var cursorRequest = store.openCursor(keyRange);

            cursorRequest.onsuccess = function (e) {
                var result = e.target.result;
                if (result === null || result === undefined) {
                    deferred.resolve(todos);
                } else {
                    todos.push(result.value);
                    if (result.value.id > lastIndex) {
                        lastIndex = result.value.id;
                    }
                    result.
                    continue ();
                }
            };

            cursorRequest.onerror = function (e) {
                console.log(e.value);
                deferred.reject('Something went wrong!!!');
            };
        }

        return deferred.promise;
    };

    var remove = function (storeName, id) {
        var deferred = $q.defer();

        if (db === null) {
            deferred.reject('IndexDB is not opened yet!');
        } else {
            var trans = db.transaction([storeName], 'readwrite');
            var store = trans.objectStore(storeName);

            var request = store.delete(id);

            request.onsuccess = function (e) {
                deferred.resolve();
            };

            request.onerror = function (e) {
                console.log(e.value);
                deferred.reject('Todo item couldn\'t be deleted');
            };
        }

        return deferred.promise;
    };

    var add = function (storeName, data, key) {
        var deferred = $q.defer();

        if (db === null) {
            deferred.reject('IndexDB is not opened yet!');
        } else {
            var trans = db.transaction([storeName], 'readwrite');
            var store = trans.objectStore(storeName);
            lastIndex++;
            console.log('data > ' + data);
            var request = store.add(data, key);
            request.onsuccess = function (e) {
              console.log(e);
                deferred.resolve();
            };

            request.onerror = function (e) {
                console.log(e.value);
                deferred.reject('Todo item couldn\'t be added!');
            };
        }
        return deferred.promise;
    };

    var edit = function (storeName, data, key) {
        var deferred = $q.defer();

        if (db === null) {
            deferred.reject('IndexDB is not opened yet!');
        } else {
            var trans = db.transaction([storeName], 'readwrite');
            var store = trans.objectStore(storeName);
            var request = store.put(data, key);
            request.onsuccess = function (e) {
              console.log(e.value)
                deferred.resolve();
            };

            request.onerror = function (e) {
                console.log(e.value);
                deferred.reject('Todo item couldn\'t be added!');
            };
        }
        return deferred.promise;
    };

    var close = function(dbName) {

      console.log(db);

      for(var i=0; i<db.objectStoreNames.length; i++) {
        // console.log(db.objectStoreNames.item(i));
        var storeName = db.objectStoreNames.item(i);
        var trans = db.transaction([storeName], 'readwrite');
        var store = trans.objectStore(storeName);
        store.clear();
      }
    }

    return {
        Open: open,
        Close: close,
        Add: add,
        Get: get,
        Edit: edit
    };

  }

})();
