/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 */
(function() {
'use strict'

  angular
    .module('mmobApp')
    .factory('SocketService', SocketService);

  SocketService.$inject = ['$rootScope', '__env'];

  function SocketService($rootScope, __env) {
    var socket = io(__env.apiUrl);

    var service = {
      On: Listen,
      Emit: Send,
      Stop: RemoveListener
    };

    function Listen(eventName, callback) {
      socket.on(eventName, function(args){
        $rootScope.$apply(function () {
          callback.apply(socket, [args]);
        });
      });
    }

    function Send(eventName, data, callback) {
      socket.emit(eventName, data, function(args){
        $rootScope.$apply(function () {
            callback.apply(socket, [args]);
        });
      });
    }

    function RemoveListener() {
      socket.removeAllListeners();
    }

    return service;

  }
})();
