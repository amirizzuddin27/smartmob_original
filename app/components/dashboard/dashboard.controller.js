/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a controller to handle the dashboard navigation, display of user name and time
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('DashboardController', DashboardController);

  DashboardController.$inject = ['$scope', '$state', '$uibModal', '$filter', 'AuthenticationService', 'PhotoManager', 'Notification', 'IndexedDBService', '$timeout'];

  function DashboardController($scope, $state, $uibModal, $filter, AuthenticationService, PhotoManager, Notification, IndexedDBService, $timeout) {
    var vm = this;
    vm.online = true;

    // Listen to changes from the server
    // SocketService.On('task:update', function(data){
    //   if(data['status'] == 1) {
    //     // console.log('data : ' + data['status']);
    //     Notification.success({message: data['desc'], title: 'Task Completed'});
    //   }
    // });

    // @brief Form and initialise the greetings for user as well as set up the IndexedDB
    //
    // @return void
    vm.Initialise = function() {
      if(AuthenticationService.IsLogon() == false)
      {
        $state.go('login');
        return;
      }

      // Get the credentials stored in the cookie
      vm.cookie = AuthenticationService.GetCredentials();
      var time = $filter('date')(new Date(), 'a');

      if(time === 'PM')
        time = 'Good Afternoon, ';
      else if(time === 'AM')
        time = 'Good Morning, ';

      // Form the greeting string for display in the view
      vm.greetings = vm.cookie.currentUser['name'];

      // Open the storage cache in indexeddb to store pictures taken on the Smart Mobility application
      IndexedDBService.Open('mmobDB', 'photos').then(function() {
        console.log('mmobDB is opened successfully');
      });

      vm.UpdateClock();
    }

    // @brief Handle the interactivity for the navigation buttons
    //
    // @param type is the type of functionality to toggle e.g. 'camera', 'defect'
    // @return void
    vm.OpenTool = function(type) {
      var modal;

      switch(type) {
        case 'camera':
        {
          console.log('Open camera function');
          modal = $uibModal.open({
                        animation: true,
                        templateUrl: 'components/camera/camera.view.html',
                        controller: 'CameraController',
                        controllerAs: 'vm',
                        windowClass: 'app-modal-window',
                        size: 'lg',
                        backdrop: 'static'
                      });
        }
        break;

        // case 'annotation':
        // {
        //   console.log('Open annotation function');
        //   modal = $uibModal.open({
        //                 animation: true,
        //                 templateUrl: 'components/annotation/annotate.view.html',
        //                 controller: 'AnnotateController',
        //                 controllerAs: 'vm',
        //                 windowClass: 'app-modal-window',
        //                 size: 'lg',
        //                 backdrop: 'static'
        //               });
        // }
        // break;

        case 'defect':
        {
          console.log('Open defect reporting function');
          modal = $uibModal.open({
                        animation: true,
                        templateUrl: 'components/defect/defect.view.html',
                        controller: 'DefectController',
                        controllerAs: 'vm',
                        windowClass: 'app-modal-window',
                        size: 'lg',
                        backdrop: 'static'
                      });
        }
        break;
      }

      // Handles the modal result if there is any
      modal.result.then(
        function() {

        },
        function() {
        }
      );
    };

    // @brief Handle the log out button
    //
    // @return void
    vm.LogOut = function() {
      AuthenticationService.LogOut();
      PhotoManager.DeleteAll();
      IndexedDBService.Close();
      $state.go('login');
    }

    // @brief Update the clocks every second
    //
    // @return void
    vm.UpdateClock = function() {
      vm.date = $filter('date') (new Date(), 'EEE, d MMM y h:mm:ss a');
      $timeout(vm.UpdateClock, 1000);
    }

    // @brief Handles the destroy event for the dashboard
    $scope.$on('$destroy', function (event) {
      console.log('dashboard > call destroy');
      // SocketService.Stop();
      vm.LogOut();
    });

    // Initialise this controller
    vm.Initialise();
  };
})();
