/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * This is a controller for the report view
 */

(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('ReportController', ReportController);

    ReportController.$inject = ['$scope', 'ReportService', 'JobcardService', '$stateParams', '$state'];


    function ReportController($scope, ReportService, JobcardService, $stateParams, $state) {
      var vm = this;

      vm.completed = $stateParams.completed;
      vm.currPage = 0;

      // @brief Retrieve the report for a jobcard. The function will use ReportService to call the report API. Data returned contains the jobcard info, list of tasks and findings associated with the task.
      //
      // @param jobcardID is the id of the jobcard the user selected
      // @return void
      vm.FindInfoByJobcardID = function(jobcardID) {
        ReportService.FindInfoByJobcardID(jobcardID).then(function(data) {
          vm.reportInfo = data[0];
          // console.log(JSON.stringify(vm.reportInfo));
          vm.tasks = data[0].tasks;
          // vm.nextPage(vm.currPage);
        });
      };

      // @brief Update the status of the jobcard. 2 - for verification, 1 - signed off or completed
      //
      // @param jobcardID is the id of the jobcard the user selected
      // @return void
      vm.SubmitReport = function(jobcard, status) {
        // console.log('Submit Report : ' + $stateParams.completed);
        JobcardService.UpdateByID(jobcard, status).then(function(data) {
          console.log(data);
        });
      }

      // vm.nextPage = function(page) {
      //   console.log(page);
      //   vm.currPage = page;
      // }

      // Initialise the controller
      if($stateParams.jobcardID == null)
        $state.go('jobcard');
      else
        vm.FindInfoByJobcardID($stateParams.jobcardID);

    }
})();
