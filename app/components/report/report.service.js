/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 */

(function() {
  'use strict';

  angular
    .module('mmobApp')
    .factory('ReportService', ReportService);

    ReportService.$inject = ['$http', '$q', '__env'];

    function ReportService($http, $q, __env) {

      var service = {
        FindInfoByJobcardID : FindInfoByJobcardID
      }
      return service;

      function FindInfoByJobcardID(jobcardID) {

        var request = $http({
          method: 'GET',
          url: __env.apiUrl + __env.baseUrl + '/report/' + jobcardID,
        });

        return(request.then(HandleSuccess,HandleError));
      };

      function HandleSuccess(response) {
        // Transform the successful response, unwrapping the application data
        // from the API response payload.
        return (response.data);
      };

      function HandleError(response) {
        // The API response from the server should be returned in a
        // nomralized format. However, if the request was not handled by the
        // server (or what not handles properly - ex. server error), then we
        // may have to normalize it on our end, as best we can.
        if (
            ! angular.isObject( response.data ) ||
            ! response.data.message
            ) {
            return( $q.reject( "An unknown error occurred." ) );
        }

        // Otherwise, use expected error message.
        return( $q.reject( response.data.message ) );
      };

    }

})();
