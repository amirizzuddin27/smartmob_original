/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a controller for the camera.view.html.
 * It handles the modal instance, display of the video from the web camera and
 * display of picture editing canvas.
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('CameraController', CameraController);

    CameraController.$inject = ['$q', '$scope', '$uibModalInstance', '$sce', 'CameraService', 'PhotoManager', '$window']

    function CameraController($q, $scope, $uibModalInstance, $sce, CameraService, PhotoManager, $window) {

      var vm = this;

      vm.showweb = false;

      // Initialise data from the webcam service
      vm.webcam = CameraService.webcam;

      // Photo manager
      vm.photoMgr = PhotoManager;
      vm.currentId = -1;
      vm.photoCount = vm.currentId;
      vm.color = 0;
      vm.previewId = -1;

      // This variable keeps track of the page state
      vm.state = 0; // 0 - camera, 1 - editor

      // Here we will initialise the webcam channel
      // vm.webcam refers to CameraService.webcam object which helps us wrap the video stream implementation
      vm.webcam.channel = {
        // the fields below are all optional
        videoHeight: 600, // Height of the video view
        videoWidth: 800, // Width of the video view
        video: null // The element for the video stream
      };

      // @brief Create and store pictures in indexedDB. Note that although this function is defined here but it has been declared with reference to CameraService.webcam as you can see below. We do this so that we can easily access the photoMgr function which is only defined in this controller.
      //
      // @param picture data such as type, height, width and base64 string
      // @return void
      vm.webcam.storeInfo = function(preview) {
        vm.photoMgr.CreatePhoto(preview);
      }

      // @brief Handle the picture capture when user presses the 'TAKE PHOTO' button. Once picture is taken, it will redirect to picture editing state
      //
      // @return void
      vm.TakePhoto = function() {
        vm.webcam.makeSnapshot();
        vm.currentId = ++vm.photoCount;
        vm.ChangeState(1);
      }

      // @brief Changes the view to either camera or editing view. 0 - camera view, 1 - editing view. When switching to editing view, we need turn off the camera stream to save and remove dangling memory because we will restart the stream again when we switch back to camera view
      //
      // @return void
      vm.ChangeState = function(state) {
        // console.log("vm.state = " + vm.state);
        // console.log("state = " + state);
        if(vm.state == 0 && vm.webcam.isTurnOn) vm.webcam.turnOff();
        // if(vm.state == 1) vm.CloseDir();

        vm.state = state;
        if(vm.state == 0) vm.currentId = -1;
      }

      // @brief Handles colour selection in editing view
      //
      // @param color - 0 (black), 1 (green), 2 (blue), 3 (red)
      // @return void
      vm.SelectColor = function(color) {
        vm.color = color;
        vm.chooseColor = false;
      }

      // @brief Handles closing of the modal window. It turns off the webcame if it is turned on and in camera view
      //
      // @return void
      vm.Close = function () {
        if(vm.state == 0) if(vm.webcam.isTurnOn) vm.webcam.turnOff();
        if(vm.state == 1) vm.CloseDir();
        $uibModalInstance.close();
      }

      // @brief Handles the drawing directive closing. Drawing directive is done in photoeditor.directive.js. It stops the directive's interval.
      //
      // @param func is the name of the function
      // @return void
      vm.CloseDirFunction = function(func) {
        vm.CloseDir = func;
      }

      vm.photoMgr.LoadFromDB();
      // vm.photoList = vm.photoMgr.GetAll();

      // IndexedDBService.Open('mmobDB', 'photos').then(function() {
      //   IndexedDBService.Get('photos').then(function(data) {
      //     console.log(data);
      //     vm.photoList = data;
      //   });
      // });

    }

})();
