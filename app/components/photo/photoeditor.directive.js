/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * This is a directive to implement the drawing element for the photo editing.
 * It uses the attribute restrict which means it can be revoked by putting the directive such as <div drawing></div>.
 * The implementation uses the canvas element 2d context to draw shapes.
 *
 */
(function (){
  'use Strict'

  angular
    .module('mmobApp')
    .directive("drawing", Drawing);

  Drawing.$inject = ['PhotoManager', '$compile', '$interval'];

  function Drawing(PhotoManager, $compile, $interval){
    return {
      restrict: 'A',
      scope: {
        selected: '=',
        cmd: '=',
        color: '=',
        choose: '=',
        closeFn: '&'
      },
      link: function(scope, element, attrs){

        // This is used to display the <input> text element on the drawing canvas.
        // We have to define this text element in the directive because we need to place it according to where we click the mouse.
        var textElement = "<div id='sketch-text'>" +
                            "<div class='input-group input-lg'>" +
                              "<input type='text' class='form-control text-lg' ng-model='desc' size='35' />" +
                              "<span class='input-group-btn'>" +
                                "<button class='btn btn-default' type='button'><span class='glyphicon glyphicon-ok-circle text-lg' ng-click='AddText()'></span></button>"+
                                "<button class='btn btn-default' type='button'><span class='glyphicon glyphicon-remove-circle text-lg' ng-click='Close()'></span></button>"+
                              "</span>"
                            "</div>"
                          "</div>";
            ngElement = angular.element(textElement);

        var curDefect, width, height;
        // 0 - default, 1 - drawings state, 2 - editing state
        // In drawing and editing states, the shapes will not be drawn in solid lines
        var cvState = 0;
        var shapeList = [];
        var drawColor = 'rgb(0,0,0)';

        // Here we create 2 canvas elements dynamically, **canvas is the real one for final drawing
        // **canvas_buffer is used only for buffering all the drawings
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        canvas.id = 'canvas';

        var canvas_buffer = document.createElement('canvas');
        var buffer_ctx = canvas_buffer.getContext('2d');
        canvas_buffer.id = 'canvas_buffer';

        buffer_ctx.font = "14px Arial";
        buffer_ctx.textAlign = "center";

        // As our directive is a div we can add child on it
        // Here we add 2 canvases we created
        element[0].appendChild(canvas);
        element[0].appendChild(canvas_buffer);

        // We need these variables to keep track of the mouse current and last positions
        var mouse = {x: 0, y: 0};
        var last_mouse = {x: 0, y: 0};
        var selectedShape;

        // Watch for changes in the selected photo on the interface
        // When user select a different preview photo, it will re-initialise the canvas with the selected photo
        scope.$watch('selected', function(newValue, oldValue) {
          if(newValue >= 0) {
            Initialise();
          }
        }, true);

        // Watch for changes in colours
        // When a new colour is selected on the user interface, we change the drawing colours
        scope.$watch('color', function(newValue, oldValue) {
          console.log(newValue);
          // rgb - red, green, blue
          // value - 0-255, 0 is black, 255 is highest for particular colour
          if(newValue == 1) { // green
            drawColor = 'rgb(128,255,128)'
          }
          else if(newValue == 2) {
            drawColor = 'rgb(128,128,255)'
          }
          else if(newValue == 3) {
            drawColor = 'rgb(255,50,50)'
          }
          else {
            drawColor = 'rgb(0,0,0)'
          }
        }, true);

        // Watch for changes in the selected commands
        // When user selects any of the tool from the bar below, cmd will change
        scope.$watch('cmd', function(newValue, oldValue) {
          if(newValue == 'delete') {
            RemoveSelected();
          } else if(newValue == 'save') {
            SaveEdit();
          } else if(newValue == 'remove') {
            PhotoManager.DeletePhoto();
          } else if(newValue == 'crop') {

          }

          if(newValue != 'select') {
            ResetSelected();
          }

        }, true);

        // Pencil Points
        var ppts = [];
        var moveStart = {x: 0, y: 0};

        /* Mouse move listener
          We need to get the mouse position as it move.
          The mouse position will be used in drawing and also moving an existing shape.
         */
        canvas_buffer.addEventListener('mousemove', function(e) {
          mouse.x = typeof e.offsetX !== 'undefined' ? e.offsetX : e.layerX;
          mouse.y = typeof e.offsetY !== 'undefined' ? e.offsetY : e.layerY;

          // If the canvas state is 2 - editing, we move shape selected according to the mouse position
          if(cvState === 2) {
            var d = {
              x: mouse.x - moveStart.x,
              y: mouse.y - moveStart.y
            };
            moveStart.x = mouse.x;
            moveStart.y = mouse.y;
            MoveShape(selectedShape, d);
          }
        }, false);

        /* Touch move listener
          Works the same way as mousemove
         */
        canvas_buffer.addEventListener('touchmove', function(e) {
          e.preventDefault();
          var touch = e.touches[0];
          var mouseEvent = new MouseEvent('mousemove', {
            clientX: touch.clientX,
            clientY: touch.clientY
          })
          canvas_buffer.dispatchEvent(mouseEvent);
        }, false);

        /* Mouse down listener
          We need to get the mouse position as user presses the mouse.
          The mouse position will be used in drawing and also checking if it collides with any existing shape while in select mode
         */
        canvas_buffer.addEventListener('mousedown', function(e) {
          // console.log('mousedown');

          mouse.x = typeof e.offsetX !== 'undefined' ? e.offsetX : e.layerX;
          mouse.y = typeof e.offsetY !== 'undefined' ? e.offsetY : e.layerY;

          // store mouse position at every click for shapes with multiple points
          ppts.push({x: mouse.x, y: mouse.y});

          scope.choose = false;

          if(scope.cmd == 'select') {
            console.log(scope.color);
            for(var i=0; i<shapeList.length; i++) {
              if(IsCollide(shapeList[i], mouse)) {
                if(shapeList[i] != selectedShape) ResetSelected();
                selectedShape = shapeList[i];
                moveStart.x = mouse.x;
                moveStart.y = mouse.y;
                cvState = 2; // editing shape
                return;
              }
            }
          }

          cvState = 1; // mouse down - start drawing

          // canvas_buffer.addEventListener('mousemove', onPaint, false);
          //onPaint();
        }, false);

        /* Touch start listener
          Works the same way as mousedown
         */
        canvas_buffer.addEventListener('touchstart', function(e) {
          e.preventDefault();
          var touch = e.touches[0];
          var mouseEvent = new MouseEvent('mousedown', {
            clientX: touch.clientX,
            clientY: touch.clientY
          });
          canvas_buffer.dispatchEvent(mouseEvent);
        }, false);

        /* Mouse up listener
          We need to get the mouse position as user releases the mouse.
          Once mouse is released in 1 - drawing canvas state, we will create permanent shape
         */
        canvas_buffer.addEventListener('mouseup', function() {

          if(cvState === 1)
          {
            ppts.push({x:mouse.x, y: mouse.y});
            AddShape(scope.cmd);
          }

          ppts = [];
          cvState = 0;

        }, false);

        /* Touch end listener
          Works the same way as mouseup
         */
        canvas_buffer.addEventListener('touchend', function(e) {
          e.preventDefault();
          var touch = e.touches[0];
          var mouseEvent = new MouseEvent('mouseup', {})
          canvas_buffer.dispatchEvent(mouseEvent);
        }, false);

        // This function will be called the moment the host that uses the directive is closed
        scope.Close = function() {
          var sketchTextEl = document.getElementById('sketch-text'),
              ngSketchTextEl = angular.element(sketchTextEl);

          sketchTextEl.value = '';
          scope.desc = '';
          ngSketchTextEl.remove();

          ppts = [];
        }

        // Put whatever text typed in the <input> element into the text object
        scope.AddText = function() {
          // shape.desc = scope.desc;
          //PhotoManager.AddDefectInfo(scope.selected, shape);

          // console.log(PhotoManager.Search(scope.selected).GetDefectData());

          var typeObject = shapeList[shapeList.length - 1];
          typeObject.text = scope.desc;
          scope.Close();
          // scope.desc = '';
        }

        // This function will initialise the drawing canvas with necessary parameters.
        // The 2 canvases will use the same dimension as the image taken.
        function Initialise() {
          // This will retrieve the image that is selected from the preview
          curDefect = PhotoManager.Search(scope.selected);

          if(curDefect == null) return;

          // width = curDefect.GetPreview().width + 2;
          // height = curDefect.GetPreview().height + 2;

          width = curDefect.width + 2;
          height = curDefect.height + 2;

          element.css('width', width + 'px');
          element.css('height', height + 'px');

          cvState = 0;
          shapeList = [];

          // Drawing canvas
          canvas.width = width;
          canvas.height = height;

          // Temporary canvas
          canvas_buffer.width = canvas.width;
          canvas_buffer.height = canvas.height;

          /* Drawing on Paint App */
          buffer_ctx.lineWidth = 2;
          buffer_ctx.lineJoin = 'round';
          buffer_ctx.lineCap = 'round';

          buffer_ctx.clearRect(0, 0, canvas_buffer.width, canvas_buffer.height);
          // ctx.putImageData(curDefect.GetRawData().data, 0, 0);

          var img = new Image;
          img.onload = function() {
            ctx.drawImage(img, 0, 0);
          }
          // img.src = "data:" + curDefect.GetPreview().photoType + ";base64," +
          //           curDefect.GetPreview().data;
          img.src = "data:" + curDefect.type + ";base64," +
                    curDefect.data;
        }

        // This function will update the canvas in intervals
        // It will only stop when the element containing the directive is closed
        function onPaint() {

          // Tmp canvas is always cleared up before drawing.
          buffer_ctx.clearRect(0, 0, canvas_buffer.width, canvas_buffer.height);

          for(var i=0; i<shapeList.length; i++) {
            DrawShape(buffer_ctx, shapeList[i]);
          }

          // console.log('onPaint');
          if(cvState === 1) DrawTempShape(buffer_ctx, scope.cmd);

        }

        // Called for drawing shape permanently
        function DrawShape(context, geometry) {
          if(geometry.type === 'circle') {
            DrawCircle(context, geometry.center, geometry.size, geometry.solid, geometry.color);
          } else if (geometry.type === 'rect') {
            DrawRect(context, geometry.start, geometry.width, geometry.height, geometry.solid, geometry.color);
          } else if (geometry.type === 'line') {
            DrawLine(context, geometry.start, geometry.end, geometry.solid, geometry.color);
          } else if(geometry.type === 'type') {
            DrawText(context, geometry);
            if(geometry.solid === false) {
              var width = context.measureText(geometry.text).width * 1.2;
              var height = geometry.fontSize * 1.2;
              var pos = {
                x: geometry.center.x - width * 0.1,
                y: geometry.center.y - geometry.fontSize
              }
              DrawRect(context, pos, width, height, geometry.solid);
            }
          }
        }

        // This draws the shape temporarily while in 1 - drawing mode
        function DrawTempShape(context, geometry) {
          var startPoint = ppts[0];
          var dx = mouse.x - startPoint.x;
          var dy = mouse.y - startPoint.y;

          if(geometry === 'circle') {
            var rad = Math.sqrt(dx*dx + dy*dy);
            DrawCircle(context, startPoint, rad, false);
          } else if (geometry === 'rect') {
            DrawRect(context, startPoint, dx, dy, false);
          } else if (geometry === 'line') {
            DrawLine(context, startPoint, mouse, false);
          }
        }

        // @brief Function to draw circle using the arc() function available in canvas context implementation
        //
        // @param context is the context to render on
        // @param center is the position of the circle
        // @param radius is the radius of the circle
        // @param solid is to indicate whether to draw in solid or not
        // @param color is the color to draw the shape
        // @return void
        function DrawCircle(context, center, radius, solid, color) {
          context.beginPath();
          context.save();

          if(solid == false) {
            context.setLineDash([5]);
            context.fillStyle = 'rgba(80,80,80,0.2)';
            context.strokeStyle = 'rgb(128, 128, 128)';
          } else {
            context.strokeStyle = color;
            context.fillStyle = 'rgba(80,80,80,0.1)';
          }

          context.arc(center.x, center.y, radius, 0, Math.PI * 2);
          context.fill();
          context.stroke();
          context.restore();
        }

        // @brief Function to draw rectangle using the fillRect() available in canvas context implementation
        //
        // @param context is the context to render on
        // @param start is the starting position of the rectangle
        // @param width is the width of the rectangle
        // @param height is the height of the rectangle
        // @param solid is to indicate whether to draw in solid or not
        // @param color is the color to draw the shape
        // @return void
        function DrawRect(context, start, width, height, solid, color) {
          context.beginPath();
          context.save();

          if(solid == false) {
            context.setLineDash([5]);
            context.fillStyle = 'rgba(80,80,80,0.2)';
            context.strokeStyle = 'rgb(128, 128, 128)';
          } else {
            context.strokeStyle = color;
            context.fillStyle = 'rgba(80,80,80,0.1)';
          }

          context.fillRect(start.x, start.y, width, height);
          context.strokeRect(start.x, start.y, width, height);
          context.restore();
        }

        // @brief Function to draw line using the lineTo() available in canvas context implementation
        //
        // @param context is the context to render on
        // @param center is the position of the circle
        // @param radius is the radius of the circle
        // @param solid is to indicate whether to draw in solid or not
        // @param color is the color to draw the shape
        // @return void
        function DrawLine(context, start, end, solid, color) {
          context.beginPath();
          context.save();

          if(solid == false) {
            context.setLineDash([5]);
            context.strokeStyle = 'rgb(128, 128, 128)';
          } else {
            context.strokeStyle = color;
          }

          context.moveTo(start.x, start.y);
          context.lineTo(end.x, end.y);
          context.stroke();
          context.restore();
        }

        // @brief Function to draw text using the fillText() available in canvas context implementation
        //
        // @param context is the context to render on
        // @param typeObject is the object which store the information about text to render
        // @return void
        function DrawText(context, typeObject) {
          typeObject.width = context.measureText(typeObject.text).width;
          context.font = typeObject.fontWeight + ' ' + typeObject.fontSize + 'px ' + typeObject.fontName;
          if(typeObject.solid === true)
            context.fillStyle = typeObject.color;
          else {
            context.fillStyle = 'rgb(128, 128, 255)';
          }
          context.fillText(typeObject.text, typeObject.center.x, typeObject.center.y);
        }

        // @brief Function to move object in a particular direction. The direction depends on the current mouse position to last move position.
        //
        // @param shape is the selected shape to move
        // @param direction is the direction to move
        // @return void
        function MoveShape(shape, direction) {
          if(shape.type == 'circle') {
            shape.center.x += direction.x;
            shape.center.y += direction.y;
          } else if(shape.type == 'rect') {
            shape.start.x += direction.x;
            shape.start.y += direction.y;
          } else if(shape.type == 'type') {
            shape.center.x += direction.x;
            shape.center.y += direction.y;
          }
        }

        // @brief Function to add and create shape. Adds the created shape to an array of objects to render.
        //
        // @param type is the type of shape to be created i.e. circle, rect, line, type (text)
        // @return void
        function AddShape(type) {
          var startPoint = ppts[0];
          var endPoint = ppts[1];
          var dx = endPoint.x - startPoint.x;
          var dy = endPoint.y - startPoint.y;

          switch(type) {
            case 'circle': // Creating circles
            {
              var rad = Math.sqrt(dx*dx + dy*dy);

              if(rad <= 1) break;

              // create circle info
              shapeList.push({
                type: type,
                center: startPoint,
                size: rad,
                solid: true,
                //color: 'rgb(255, 128, 128)'
                color: drawColor
              });
            }
            break;

            case 'rect': // Creating rectangle
            {
              // create rect info
              shapeList.push({
                type: type,
                start: startPoint,
                width: dx,
                height: dy,
                center: {
                  x: startPoint.x + dx * 0.5,
                  y: startPoint.y + dy * 0.5,
                },
                solid: true,
                //color: 'rgb(255, 128, 128)'
                color: drawColor
              });
            }
            break;

            case 'line': // Drawing lines
            {
              var rad = dx*dx + dy*dy;
              if(rad <= 1) return;

              // create rect info
              shapeList.push({
                type: type,
                start: startPoint,
                end: endPoint,
                center: {
                  x: startPoint.x + dx * 0.5,
                  y: startPoint.y + dy * 0.5,
                },
                solid: true,
                //color: 'rgb(255, 128, 128)'
                color: drawColor
              });
            }
            break;

            case 'type':
              ngElement.css('top', startPoint.y + 'px');
              ngElement.css('left', (startPoint.x - 100) + 'px');
              element.append(ngElement);
              $compile(ngElement)(scope);

              shapeList.push({
                type: type,
                center: {
                  x: startPoint.x,
                  y: startPoint.y
                },
                text: '',
                width: 0,
                fontSize: 40, // default to 20
                fontName: 'Roboto-Bold', // default to Roboto-Light
                fontWeight: 'bold',
                //color: 'rgb(255, 128, 128)'
                color: drawColor,
                solid: true
              });
            break;
          }
        }

        // This function does simple collision checks for the different kind of shapes with a position
        // This is called when mouse is clicked to select object
        function IsCollide(shape, position) {
          var result = false;

          if(shape.type == 'circle') {
            // Circle uses the distance comparison
            //
            var dx = position.x - shape.center.x;
            var dy = position.y - shape.center.y;
            var d = dx * dx + dy * dy;

            result = (d <= shape.size * shape.size);
            if(result == true) shape.solid = false;

          } else if(shape.type == 'rect') {
            // Rectangle collision checks if position is within left and right range of the rectangle,
            // as well as top and bottom range of the rectangle

            var dx = (position.x >= shape.start.x && position.x <= shape.start.x + shape.width);
            var dy = (position.y >= shape.start.y && position.y <= shape.start.y + shape.height);

            result = (dx && dy);
            if(result == true) shape.solid = false;

          } else if(shape.type == 'line') {
            //console.log('line');
          } else if(shape.type == 'type') {
            // Rectangle collision checks if position is within left and right range of the rectangle,
            // as well as top and bottom range of the rectangle

            var dx = (position.x >= shape.center.x && position.x <= shape.center.x + shape.width);
            var dy = (position.y >= shape.center.y - shape.fontSize * 0.8 && position.y <= shape.center.y + shape.fontSize * 0.8);

            result = (dx && dy);
            if(result == true) {
              shape.solid = false;
            }
          }

          return result;
        }

        // @brief Function to reset selected shape to solid when you no longer select it.
        //
        // @return void
        function ResetSelected() {

          if(!selectedShape) return;

          selectedShape.solid = true;
          // if(selectedShape.type == 'type') selectedShape.color = 'rgb(255, 128, 128)';
        }

        // @brief Function to erase a selected shape from the canvas before it is saved.
        //
        // @return void
        function RemoveSelected() {
          if(selectedShape == null) return;

          var i = shapeList.indexOf(selectedShape);
          if(i > -1) shapeList.splice(i, 1);
        }

        // @brief Function to save the edited canvas. Image will be saved as cache in Base64 format.
        //
        // @return void
        function SaveEdit() {
          // Remove all shapeList
          ResetSelected();
          onPaint();

          shapeList = [];

          // Writing down to real canvas now
          ctx.drawImage(canvas_buffer, 0, 0);
          // Clearing tmp canvas
          buffer_ctx.clearRect(0, 0, canvas_buffer.width, canvas_buffer.height);

          // Create prevew thumbnail
          // Here we save the preview data the same way we save th picture taken using camera
          var snapshotData = canvas.toDataURL('image/png');
          var preview = {
            data: snapshotData.substr(snapshotData.indexOf('base64,') + 'base64,'.length),
            type: 'image/png',
            width: width,
            height: height
          };

          // Create raw image data
          // var raw = {
          //   width: width,
          //   height: height,
          //   data: ctx.getImageData(0, 0, width, height)
          // }

          PhotoManager.ModifyPhoto(scope.selected, preview);
        }

        // We need to constantly update our drawing canvas.
        // we create an interval function to update the drawing canvas.
        Draw = $interval(function(){
          onPaint();
        }, 17); // Draw at ~60fps

        // @brief Function to stop the interval. This is to be called when the page or element that contains this directive is closed. It removes the dangling intervale function from the memory.
        //
        // @return void
        StopInterval = function(){
          $interval.cancel(Draw);
        }

        scope.closeFn({func: StopInterval});
      }
    }
  };

})();
