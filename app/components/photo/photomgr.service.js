/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 */
(function (){
  'use Strict'

  angular
    .module('mmobApp')
    .factory('PhotoManager', PhotoManager);

  PhotoManager.$inject = ['$q', '$filter', 'Photo', 'IndexedDBService']

  function PhotoManager($q, $filter, Photo, IndexedDBService) {
    var photoList = [];

    var photoMgr = {
      CreatePhoto: CreatePhoto,
      CreatePhotoFromURL: CreatePhotoFromURL,
      ModifyPhoto: ModifyPhoto,
      DeletePhoto: DeletePhoto,
      DeleteAll: DeleteAll,
      Search: Search,
      GetAll: GetAll,
      LoadFromDB: LoadFromDB
    }

    function CreatePhoto(preview) {
      // photoList.push(new Photo(preview, raw));
      var photo = new Photo($filter('date')(new Date(), 'yyyyMMddHHmmss'),
                            preview.data,
                            preview.type,
                            preview.width,
                            preview.height);
      photoList.unshift(photo);
      console.log('photo > ' + JSON.stringify(photo));

      var data = {
        photoid: photo.photoid,
        data: photo.data,
        type: photo.type,
        width: photo.width,
        height: photo.height
      }

      IndexedDBService.Add('photos', data, photo['photoid']).then(function() {
        console.log('Added new photo successfully');
      });
    }

    function CreatePhotoFromURL(url) {
      var image = new Image();
      image.setAttribute('crossOrigin', 'anonymous');

      image.onload = function () {
          var canvas = document.createElement('canvas');
          canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
          canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

          canvas.getContext('2d').drawImage(this, 0, 0);

          console.log(canvas.width + ', ' + canvas.height);

          console.log(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

          var preview = {
            data: canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''),
            photoType: 'image/png'
          };

          // Get raw image data
          // callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

          // ... or get as Data URI
          // callback(canvas.toDataURL('image/png'));
      };

      image.src = url;
    }

    // function ModifyPhoto(id, preview, raw) {
    function ModifyPhoto(id, preview) {
      var photo = Search(id);
      // photo.SetPreview(preview);
      // photo.SetRaw(raw);
      photo.Edit(preview.data, preview.type, preview.width, preview.height);

      var data = {
        photoid: photo.photoid,
        data: preview.data,
        type: preview.type,
        width: preview.width,
        height: preview.height
      }

      // IndexedDBService.Edit('photos', JSON.stringify(photo), photo['photoid']).then(function() {
      IndexedDBService.Edit('photos', data, photo["photoid"]).then(function() {
        console.log('Added new photo successfully');
      });
    }

    function DeletePhoto(id) {
      photoList.splice(id, 1);
    }

    function DeleteAll() {
      photoList = [];
    }

    function Search(id) {
      if(photoList.length < 0 || id > photoList.length)
        return null;

      return photoList[id];
    }

    function LoadFromDB() {
      IndexedDBService.Get('photos').then(function(data) {

        console.log("count photos > " + data.length);

        if(data.length > 0) {
          photoList = data.map(function(item) {
            var photo = new Photo(item.photoid, item.data, item.type, item.width, item.height);
            return photo;
          });
          // console.log(data);
          photoList.reverse();
        }
      });
    }

    function GetAll() {
      return photoList;
    }

    return photoMgr;
  }

})();
