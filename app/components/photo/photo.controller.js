/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * This file may no longer be used in the project
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('PhotoController', PhotoController);

    PhotoController.$inject = ['$scope', '$uibModalInstance', 'PhotoManager']

    function PhotoController($scope, $uibModalInstance, PhotoManager) {

      var vm = this;

      vm.currentId = 0;

      // Initialise data from the webcam service
      vm.photoMgr = PhotoManager;

      // Close the modal window
      vm.Close = function () {
        $uibModalInstance.close();
      }
    }

})();
