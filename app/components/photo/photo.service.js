/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * This is a service to store the information for the photo
 *
 */
(function (){
  'use Strict'

  angular
    .module('mmobApp')
    .factory('Photo', Photo);

  Photo.$inject = ['$http', '$filter'];

  function Photo($http, $filter) {


    Photo = function(photoid, data, type, width, height) {
      this.photoid = photoid;
      this.width = width;
      this.height = height;
      this.data = data;
      this.type = type;

      this.Edit = function(data, type, width, height) {
        this.width = width;
        this.height = height;
        this.data = data;
        this.type = type;
      }
    }

    return Photo;
  }

})();
