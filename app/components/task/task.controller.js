/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a controller to handle the task view
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('TaskController', TaskController);

  TaskController.$inject = ['$scope', '$state', '$http', '$stateParams', '$uibModal', 'TaskService', 'Notification', '$q', '$window', 'AuthenticationService', 'Base64', 'SocketService'];

  function TaskController($scope, $state, $http, $stateParams, $uibModal, TaskService, Notification, $q, $window, AuthenticationService, Base64, SocketService) {

    var vm = this;

    vm.cookie = AuthenticationService.GetCredentials();
    vm.jobcard = $stateParams.jobcard;
    vm.filters = [0, 1, 2];

    // Listen to changes from the server
    SocketService.On('task:update', function(data){
      if(data['status'] == 1) {
        // console.log('data : ' + data['status']);
        Notification.success({message: data['desc'], title: 'Task Completed'});
      }

      // console.log(data['jobcard_id']);
      vm.ListTaskByJobcard(vm.jobcard['id']);
    });

    // @brief List tasks based on the jobcard ID
    //
    // @param jobcardID is the id of the jobcard the user selected
    // @return void
    vm.ListTaskByJobcard = function(jobcardID) {

      TaskService.ListByJobcardID(jobcardID).then(function(data){
        vm.listOfJC = data;
      });
    }

    // @brief Update task status
    //
    // @param task is task data to update
    // @return void
    vm.UpdateStatus = function(task) {
      console.log(task)

      console.log("tasks : " + angular.fromJson(task));

      TaskService.UpdateByID(task).then(function (data) {
        vm.ListTaskByJobcard(vm.jobcard["id"]);

        // If user clicks on start task and update the status to 'in progress'
        // check if there is external web application to start
        // If there is, open in new tab/window
        if(data['status'] == 2) {
          if(task['app_url'] != "" && task['app_url'] != null) {

            var url = task['app_url'] + '&taskid=' + task['id'];
            console.log(url);
            $window.open(url, '_blank');
          }
        }
      });
    }

    // @brief Opens a modal window to see the list of tools available
    //
    // @param taskid is the id of the task which you want to list the tools
    // @return void
    vm.CheckTool = function(taskid) {
      var modal = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/dashboard-worker/tools/tool.view.html',
                    controller: 'ToolController',
                    controllerAs: 'vm',
                    windowClass: 'app-modal-window',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                      taskid: function() { return taskid; }
                    }
                  });
    }

    // @brief Opens a modal window for attaching findings related to the task
    //
    // @param taskid is the id of the task which you want to attach the finding to
    // @return void
    vm.AttachFinding = function(taskid) {
      console.log('taskid > ' + taskid);
      var modal = $uibModal.open({
                    animation: true,
                    templateUrl: 'components/task-finding/finding.view.html',
                    controller: 'FindingController',
                    controllerAs: 'vm',
                    windowClass: 'app-modal-window',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                      taskid: function() { return taskid; }
                    }
                  });
    }

    // @brief Handles the report finding button
    //
    // @param task is the task data which you want to attach the finding to
    // @param type is the type of finding
    // @return void
    vm.AddFinding = function(task, type) {
      var finding = {
        type: type
      }
      console.log(finding)

      if(!task['findings']) task['findings'] = [];

      switch(type) {
        case 'NOTE':
          finding['item'] = "";
          task['findings'].push(finding);
        break;

        case 'PHOTO':
          console.log(task);
          vm.AttachFinding(task['id']);
        break;
      }
    }

    // @brief Adds the status to the 'filters' array for processing by taskFilter function defined in app.module.js
    //
    // @param status is the status of the task to filter
    // @return void
    vm.HandleFilter = function(status) {
     
      
      let index = vm.filters.indexOf(status);
      console.log("HandleFilter > status > " + index);
      if(index == -1)
        vm.filters.push(status);
      else {
        vm.filters.splice(index, 1);
      }
    }

    // This is to keep track of when the controller is destroyed.
    // Need to remove any dangling memory e.g. SocketService listeners
    $scope.$on('$destroy', function (event) {
      SocketService.Stop();
      console.log('call destroy');
    });

    // @brief Initialize the task controller
    //
    // @return void
    vm.Initialize = function() {
      console.log('here');

      if(vm.jobcard == null)
        $state.go('jobcard');
      else
        vm.ListTaskByJobcard(vm.jobcard['id']);
    }

    vm.Initialize();
  }
}) ();
