/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a service to connect to Task API
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .factory('TaskService', TaskService);

  TaskService.$inject = ['$http', '$q', '__env'];

  function TaskService($http, $q, __env) {

    var service = {
      ListByJobcardID : ListByJobcardID,
      UpdateByID : UpdateByID
    }

    return service;

    // @brief List all the task under a particular jobcard ID
    //
    // @param jobcardID is the id of the user log on to the application
    // @return promise to the HTTP request, caller must handle the promise
    function ListByJobcardID(jobcardID) {

      var request = $http({
                            method: 'GET',
                            url: __env.apiUrl + __env.baseUrl + '/jobcard/' + jobcardID + '/tasks',
                          });

      return (request.then(HandleSuccess, HandleError));
    };

    // @brief Update task status
    //
    // @param task is the task data to update
    // @return promise to the HTTP request, caller must handle the promise
    function UpdateByID(task) {
      var status = task['status'];


      if(status === 1) return;
      if(status === 0) status = 2;
      else if(status === 2) status = 1;

      task['status'] = status;

      var request = $http({
                            method: 'PUT',
                            url: __env.apiUrl + __env.baseUrl + '/task/' + task['id'] +'/status',
                            data: angular.toJson(task),
                            headers: {
                              'Content-Type' : 'application/json'
                            }
                          });

      return (request.then(HandleSuccess, HandleError));
    };

    // Get jobcard status by jobcardID
    // function GetStatusByID(jobcardID) {
    //
    //     var request = $http({
    //                           method: 'GET',
    //                           url: __env.apiUrl + __env.baseUrl + '/jobcard/' + jobcardID + '/status'
    //                         });
    //
    //     return (request.then(HandleSuccess, HandleError));
    // }


    function HandleSuccess(response) {
      // Transform the successful response, unwrapping the application data
      // from the API response payload.
      return (response.data);
    };

    function HandleError(response) {
      // The API response from the server should be returned in a
      // nomralized format. However, if the request was not handled by the
      // server (or what not handles properly - ex. server error), then we
      // may have to normalize it on our end, as best we can.
      if (
          ! angular.isObject( response.data ) ||
          ! response.data.message
          ) {
          return( $q.reject( "An unknown error occurred." ) );
      }

      // Otherwise, use expected error message.
      return( $q.reject( response.data.message ) );
    };

  }

})();
