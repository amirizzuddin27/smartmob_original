/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a controller to handle declaration of ad-hoc finding
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('DefectController', DefectController);

    DefectController.$inject = ['$scope', '$timeout', '$uibModalInstance', '$cookieStore', '$filter', 'PhotoManager', 'FindingManager'];

    function DefectController($scope, $timeout, $uibModalInstance, $cookieStore, $filter, PhotoManager, FindingManager) {
      var vm = this;
      vm.defectList = [];
      vm.selectedPhoto = [];

      vm.acreg = [];

      vm.formPage = 1;
      vm.cookie = $cookieStore.get('globals');
      vm.photoMgr = PhotoManager;
      vm.findingMgr = FindingManager;

      vm.today = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
      vm.message = "";
      vm.submit = false;

      // @brief Check if particular picture has been selected or not
      //
      // @param photo is the picture object to check
      // @return true or false
      vm.IsPhotoSelected = function(photo) {
        var result = vm.selectedPhoto.indexOf(photo);
        // console.log('result > ' + result);
        if(result > -1) return true;
        return false;
      }

      // @brief Handles select and deselect of picture from list. If it's already in selected pictures, remove from it. otherwise, add to the list
      //
      // @param photo is the picture object to select/deselect
      // @return void
      vm.SelectPhoto = function(photo) {
        if(vm.IsPhotoSelected(photo) === true) {
          vm.RemoveSelected(photo);
        } else {
          vm.selectedPhoto.push(photo);
        }
      }

      // @brief Handle removal of picture from list
      //
      // @param photo is the picture object to remove
      // @return void
      vm.RemoveSelected = function(photo) {
        var id = vm.selectedPhoto.indexOf(photo);
        vm.selectedPhoto.splice(id, 1);
      }

      // @brief Add defect to the list
      //
      // @return void
      // vm.AddDefect = function() {
      //   if(vm.defectList.length >=5 ) return;
      //
      //   vm.defectList.push(
      //     {desc: 'test', action: 'fix it'}
      //   );
      // }


      // Finding submission data
      vm.findingDesc = '';
      vm.acReg = '';

      // @brief Handle submission of the finding. When successful, the modal window will close after 3 seconds.
      //
      // @return void
      vm.Submit = function() {

        // If there is no aircraft id selected, do not process
        if(vm.acReg == null) {
          vm.message = "You have not selected the aircraft";
          return;
        }

        // Create a finding for each picture attached
        vm.selectedPhoto.forEach(function(item, index) {
          if(vm.findingDesc == null) vm.findingDesc = '';
          vm.findingMgr.Create('', item, vm.findingDesc, vm.acReg);
        })

        // Store the data to the server and upload all the pictures
        vm.findingMgr.Submit()
        .then(function(data) {
          // console.log(result.data);
          vm.message = vm.selectedPhoto.length + " finding(s) added";
          vm.findingMgr.Clear();
          $timeout(vm.Close(), 5000);
        })
        .catch(function(error) {
          vm.message = error;
        });
      }

      // @brief Handle removal of defect selection
      //
      // @param id is the index of the defect list
      // @return void
      // vm.RemoveDefect = function(id) {
      //   vm.defectList.splice(id, 1);
      // }

      // @brief Handles closing of the modal window.
      //
      // @return void
      vm.Close = function () {
        $uibModalInstance.close();
      }

      // @brief Initialise the defect declaration modal
      //
      // @return void
      vm.Initialise = function() {
        // DefectService.GetAircraftReg().then(function(data) {
        //   vm.acreg = data.Result;
        //   console.log('list of A/C : ' + vm.acreg);
        // })
      }

      // Call to initialise controller
      vm.photoMgr.LoadFromDB();
      vm.Initialise();

    }

})();
