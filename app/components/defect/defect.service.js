/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * This file may no longer be in use and left behind with comments
 *
 */
// (function() {
//   'use strict';
//
//   angular
//     .module('mmobApp')
//     .factory('DefectService', DefectService);
//
//   DefectService.$inject = ['$http', '$q', '__env'];
//
//   function DefectService($http, $q, __env) {
//
//
//
//     var service = {
//       GetAircraftReg : GetAircraftReg,
//       SubmitFinding : SubmitFinding
//     }
//
//     return service;
//
//     function GetAircraftReg() {
//       var request = $http({
//         method: 'GET',
//         url: __env.haofuApiUrl + 'reg'
//       });
//
//       return (request.then(HandleSuccess, HandleError));
//     }
//
//     function SubmitFinding(submitData) {
//
//       console.log(angular.toJson(submitData));
//
//       var request = $http({
//         method: 'POST',
//         url: __env.haofuApiUrl + 'finding',
//         data: angular.toJson(submitData),
//         headers: {
//           'Content-Type' : 'application/json'
//         }
//       });
//
//       return (request.then(HandleSuccess, HandleError));
//     }
//
//     function HandleSuccess(response) {
//       // Transform the successful response, unwrapping the application data
//       // from the API response payload.
//       return (response.data);
//     };
//
//     function HandleError(response) {
//       // The API response from the server should be returned in a
//       // nomralized format. However, if the request was not handled by the
//       // server (or what not handles properly - ex. server error), then we
//       // may have to normalize it on our end, as best we can.
//       if (
//           ! angular.isObject( response.data ) ||
//           ! response.data.message
//           ) {
//           return( $q.reject( "An unknown error occurred." ) );
//       }
//
//       // Otherwise, use expected error message.
//       return( $q.reject( response.data.message ) );
//     };
//
//   }
//
// })();
