/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 */
(function (){
  'use Strict'

  angular
    .module('mmobApp')
    .factory('FindingManager', FindingManager);

  FindingManager.$inject = ['$http', '$filter', 'Finding', 'PhotoManager', '__env'];

  function FindingManager($http, $filter, Finding, PhotoManager, __env) {
    var list = [];

    var service = {
      Submit: Submit,
      Create: Create,
      GetAll: GetAll,
      Retrieve: Retrieve,
      RetrieveAll: RetrieveAll,
      UpdateFinding: UpdateFinding,
      RemoveByID: RemoveByID,
      Clear: Clear
    }

    function Create(taskid, photo, desc, acreg) {
      // console.log(photo);
      list.push(new Finding(taskid, photo, desc, acreg));
    }

    function GetAll() {
      // var exist = Ret


      return list;
    }

    function Clear() {
      list = [];
    }

    function Retrieve(taskid) {
      console.log(taskid)
      var request = $http({ method: 'GET',
                            url: __env.apiUrl + __env.baseUrl + '/task/' + taskid + '/findings',
                          });

      return (request.then(HandleSuccess, HandleError));
    }

    function RetrieveAll() {
      var request = $http({ method: 'GET',
                            url: __env.apiUrl + __env.baseUrl + '/findings',
                          });

      return (request.then(HandleSuccess, HandleError));
    }

    function Submit() {
      var postData = [];

      for(var i=0; i<list.length; i++) {
        var finding = list[i];
        var data = {
          task_id: finding.GetTaskID(),
          photo: finding.GetPhoto().data,
          desc: finding.GetDescription(),
          key: finding.GetAircraftReg() + '/' + $filter('date')(new Date(), 'yyyyddMMHHmmss_') + i + '.png'
        }

        console.log('data : ' + finding.GetAircraftReg());

        postData.push(data);
      }

      var request = $http({ method: 'POST',
                            url: __env.apiUrl + __env.baseUrl + '/finding/bulk',
                            headers: {
                              'Content-Type': 'application/json'
                            },
                            data: postData
                          });

      return (request.then(HandleSuccess, HandleError));
    }

    function UpdateFinding(updates) {
      console.log(updates);

      var request = $http({ method: 'PUT',
                            url: __env.apiUrl + __env.baseUrl + '/finding/bulk',
                            headers: {
                              'Content-Type': 'application/json'
                            },
                            data: angular.toJson(updates)
                          });

      return (request.then(HandleSuccess, HandleError));
    }

    function RemoveByID(id) {
      console.log(id);

      var request = $http({ method: 'DELETE',
                            url: __env.apiUrl + __env.baseUrl + '/finding/' + id
                          });

      return (request.then(HandleSuccess, HandleError));
    }

    function HandleSuccess(response) {
      // Transform the successful response, unwrapping the application data
      // from the API response payload.
      return (response.data);
    }

    function HandleError(response) {
      // The API response from the server should be returned in a
      // nomralized format. However, if the request was not handled by the
      // server (or what not handles properly - ex. server error), then we
      // may have to normalize it on our end, as best we can.
      if (
          ! angular.isObject( response.data ) ||
          ! response.data.message
          ) {
          return( $q.reject( "An unknown error occurred." ) );
      }

      // Otherwise, use expected error message.
      return( $q.reject( response.data.message ) );
    }

    return service;
  }

})();
