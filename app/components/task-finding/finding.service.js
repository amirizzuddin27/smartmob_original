/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is the data model for the finding object
 *
 */
(function (){
  'use Strict'

  angular
    .module('mmobApp')
    .factory('Finding', Finding);

  Finding.$inject = [];

  function Finding($http) {
    Finding = function(taskid, photo, desc, acreg) {
      this.taskid = taskid;
      this.photo = photo;
      this.desc = desc;
      this.acreg = acreg;

      this.GetTaskID = function() { return this.taskid; }
      this.GetPhoto = function() { return this.photo; }
      this.GetDescription = function() { return this.desc; }
      this.GetAircraftReg = function() { return this.acreg; }
      //
      // this.SetPhoto = function(photo) {
      //   this.photo = photo;
      // }
      //
      // this.SetDescription = function(desc) {
      //   this.desc = desc;
      // }


    }

    return Finding;
  }

})();
