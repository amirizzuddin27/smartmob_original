/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a controller to handle the finding related to task
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('FindingController', FindingController);

    FindingController.$inject = ['$scope', '$uibModalInstance', 'PhotoManager', 'FindingManager', 'taskid'];

    function FindingController($scope, $uibModalInstance, PhotoManager, FindingManager, taskid) {
      var vm = this;
      vm.selectedPhoto = [];
      vm.attached = [];
      vm.serverPhoto = [];
      vm.selectedServerPhoto = [];
      vm.desc = [];

      // This is used to indicate the page for Finding view.
      // 0 - show list of findings already attached, 1 - show list of photos in indexedDB
      vm.formPage = 0;

      vm.photoMgr = PhotoManager;
      vm.findingMgr = FindingManager;

      // Display messages
      vm.message = "";

      // console.log(vm.photoMgr.GetAll());
      vm.submit = false;

      vm.SelectServerPhoto = function(photo) {
        console.log('here');
        if(vm.IsServerPhotoSelected(photo) === true) {
          vm.RemoveSelected(photo);
        } else {
          vm.selectedServerPhoto.push(photo);
        }
      }

      vm.IsServerPhotoSelected = function(photo) {
        var result = vm.selectedServerPhoto.indexOf(photo);

        return (result > -1);
      }

      vm.RemoveSelectedServerPhoto = function(photo) {
        var id = vm.selectedServerPhoto.indexOf(photo);
        vm.selectedServerPhoto.splice(id, 1);
      }

      // @brief Handles select and deselect of picture from list. If it's already in selected pictures, remove from it. otherwise, add to the list
      //
      // @param photo is the picture object to select/deselect
      // @return void
      vm.SelectPhoto = function(photo) {
        if(vm.IsPhotoSelected(photo) === true) {
          vm.RemoveSelected(photo);
        } else {
          vm.selectedPhoto.push(photo);
        }
      }

      // @brief Check if particular picture has been selected or not
      //
      // @param photo is the picture object to check
      // @return true or false
      vm.IsPhotoSelected = function(photo) {
        var result = vm.selectedPhoto.indexOf(photo);
        console.log('result > ' + result);
        if(result > -1) return true;
        return false;
      }

      // @brief Handle removal of picture from list
      //
      // @param photo is the picture object to remove
      // @return void
      vm.RemoveSelected = function(photo) {
        var id = vm.selectedPhoto.indexOf(photo);
        vm.selectedPhoto.splice(id, 1);
      }

      // @brief Handle logic to submit finding(s) to the server. It creates a list of findings based on task id before submission to server.
      //
      // @return void
      vm.RegisterFinding = function() {

        // vm.submit is used as a condition for the submit button in the view
        // when set to true, the button is disabled
        vm.submit = true;

        // Create a finding for each of the picture in findingMgr
        vm.selectedPhoto.forEach(function(item, index) {
          if(vm.desc[index] == null) vm.desc[index] = '';
          vm.findingMgr.Create(taskid, vm.selectedPhoto[index], vm.desc[index], __env.acReg);
        });

        // Submit all the finding data to the server
        vm.findingMgr.Submit()
        .then(function(data) {
          // Display the number of findings successfuly added to the server
          vm.message = vm.selectedPhoto.length + " finding(s) added";
          vm.findingMgr.Clear();

          // Reload the list of findings if any in the view after submission is successful
          vm.Load();

          // enable the submit button
          vm.submit = false;
        })
        .catch(function(error) {
          vm.message = error;
        });
      }

      // @brief Handle logic to remove findings from the server.
      //
      // @param finding is the object we want to remove
      // @return void
      vm.RemoveFinding = function(finding) {
        console.log('finding : ' + finding['id']);
        vm.findingMgr.RemoveByID(finding['id']).then(function(data) {
          vm.Load();
        })
      }

      // @brief Handle logic to update findings from the server.
      //
      // @return void
      vm.UpdateFinding = function() {
        var updates = [];
        for(var i=0; i<vm.selectedServerPhoto.length; i++) {
          updates[i] = vm.selectedServerPhoto[i];
          updates[i].task_id = taskid;
          updates[i].desc = vm.desc[i];
        }

        vm.findingMgr.UpdateFinding(updates).then(function(data) {
          vm.Load();
        });
      }

      // @brief Loads the existing finding based on taskid
      //
      // @return void
      vm.Load = function() {

        // Retrieve the findings that have been associated with task
        // The data returned is in array form which we can use ng-repeat to loop and display
        vm.findingMgr.Retrieve(taskid).then(function(data) {
          vm.attached = data;
          console.log(vm.attached)

          // If there are findings attached to a task already, display them in formPage 0
          // Else go to formPage 1 to display images from indexedDB
          if(vm.attached.length <= 0) {
            vm.formPage = 1;

            // photoMgr manages the list of photos stored in IndexedDB
            // Here we load whatever them so that we can display for user selection in the view
            vm.photoMgr.LoadFromDB();
          } else {
            vm.formPage = 0;
          }

          console.log('finding page > ' + vm.formPage);
        });

        // vm.findingMgr.RetrieveAll().then(function(data) {
        //   vm.serverPhoto = data;
        // })
      }

      // @brief Handle the different button commands used in the modal
      //
      // @param cmd is the command for the button
      // @return void
      vm.HandleCommand = function(cmd) {
        switch(cmd) {
          case "BACK":
          {
            // Ensure that clicking back button only works for formPage == 1
            // Other pages will not work
            (vm.formPage == 1) ? vm.formPage = 0 : vm.formPage = 1;
            vm.selectedPhoto = [];
          }
          break;

          case "ADD":
          {
            vm.formPage = 1;

            // photoMgr manages the list of photos stored in IndexedDB
            // Here we load whatever them so that we can display for user selection in the view
            vm.photoMgr.LoadFromDB();
          }
          break;

          case "NEXT":
          {
            vm.formPage = 2;
          }
          break;

          case "SUBMIT":
          {
            vm.RegisterFinding();
          }
          break;
        }
      }

      // @brief Handles closing of the modal window.
      //
      // @return void
      vm.Close = function () {
        $uibModalInstance.close();
      }

      vm.Load();
    }

})();
