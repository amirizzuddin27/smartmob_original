/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a service to connect to login API
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .factory('AuthenticationService', AuthenticationService)


  AuthenticationService.$inject = ['Base64', '$http', '$cookieStore', '$rootScope', '$timeout', '__env'];

  function AuthenticationService(Base64, $http, $cookieStore, $rootScope, $timeout, __env) {
    var service = {
      Login : Login,
      GetCredentials : GetCredentials,
      SetCredentials : SetCredentials,
      IsLogon : IsLogon,
      LogOut : LogOut
    };

    return service;

    // @brief Authenticate user
    //
    // @param email is the email address used as user id
    // @param password is the password of the user
    // @return promise to the HTTP request, caller must handle the promise
    function Login(email, password) {

      var params = {id: email, password: password};

      var request = $http({
                      method: 'POST',
                      url: __env.apiUrl + __env.baseUrl + '/user/',
                      data: angular.toJson(params),
                      // crossDomain:true,
                      headers: {
                        'Content-Type' : 'application/json'
                      }
                    });

      return (request.then(HandleSuccess, HandleError));
    };

    // @brief Get authenticated user credentials from the cookie
    //
    // @return user data
    function GetCredentials() {
      return $cookieStore.get('globals');
    }

    // @brief Store authenticated user credentials into the cookie
    //
    // @param user is the user data retrieved from database
    // @return void
    function SetCredentials(user) {
      var authdata = Base64.encode(user.id + ':' + user.password);

      var user = {
        currentUser: {
          email : user.id,
          name: user.firstName + ' ' + user.lastName,
          role: user.role,
          authdata: authdata
        }
      };

      $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
      $cookieStore.put('globals', user);

    };

    // @brief Log user out of the application and remove all the credentials from the $cookieStore
    //
    // @return void
    function LogOut() {
      $cookieStore.remove('globals');
    }

    // @brief Check if user is already authenticated
    //
    // @return true or false
    function IsLogon() {
      return ($cookieStore.get('globals') != null)
    }

    function HandleSuccess(response) {
      // Transform the successful response, unwrapping the application data
      // from the API response payload.
      return (response.data);
    };

    function HandleError(response) {
      // The API response from the server should be returned in a
      // nomralized format. However, if the request was not handled by the
      // server (or what not handles properly - ex. server error), then we
      // may have to normalize it on our end, as best we can.
      if (
          ! angular.isObject( response.data ) ||
          ! response.data.message
          ) {
          return( $q.reject( "An unknown error occurred." ) );
      }

      // Otherwise, use expected error message.
      return( $q.reject( response.data.message ) );
    };


  }

}) ();
