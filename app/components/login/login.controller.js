/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a controller to handle the login view
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$scope', '$state', '$cookieStore', '$window', 'AuthenticationService','Notification', 'IndexedDBService'];

  function LoginController($scope, $state,  $cookieStore, $window, AuthenticationService, Notification, IndexedDBService) {
    var vm = this;

    vm.email = "";
    vm.error = "";

    // @brief Authenticate the user and redirect the page according to role
    //
    // @return void
    vm.Authenticate = function() {
      // AuthenticationService
      var dataLoading = true;
      AuthenticationService.Login(vm.email, vm.password)
      .then(result => {
        console.log(result);
        if(result) {

          // Base64 conversion
          AuthenticationService.SetCredentials(result);

          // Re-direct to dashboards
          vm.RedirectByRole(result.role);

          return;

        }

        vm.error = 'The username or password you have entered is not valid';
      })
      .catch(error => {
        vm.error = 'There seems to be something wrong with our system. Please contact our administrator.';
      });
    }

    // @brief This is a utility function to redirect page based on role
    //
    // @param role is the type of user role
    // @return void
    vm.RedirectByRole = function(role) {
      // Re-direct to dashboards
      switch(role) {
        case 1:
          // console.log('Username: ' + isAuthenticated.data.id + "\n" + 'Password: ' + isAuthenticated.data.password);
          $state.go('dashboard-supervisor');
          break;

        case 2:
          // console.log('Username: ' + isAuthenticated.data.id + "\n" + 'Password: ' + isAuthenticated.data.password);
          $state.go('dashboard');
          break;

        default:
          console.log('Username: ' + isAuthenticated);
      }
    };

    // @brief This function is used to reset the form data
    //
    // @return void
    vm.Reset = function() {

      vm.email = ''
      vm.password = ''
    };

    // Check if user is log on already
    if(AuthenticationService.IsLogon())
    {
      var currentUser = AuthenticationService.GetCredentials();

      // console.log(currentUser.currentUser[]);
      vm.RedirectByRole(currentUser.currentUser['role']);
    }
  }

})();
