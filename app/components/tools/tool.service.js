/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is the service to connect to Tool API
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .factory('ToolService', ToolService);

  ToolService.$inject = ['$http', '$q', '$cookieStore', '$filter'];

  function ToolService($http, $q, $cookieStore, $filter) {

    var service = {
      FindToolsByTaskID : FindToolsByTaskID,
      CheckToolsAvailability : CheckToolsAvailability,
      Authenticate : Authenticate,
      ReserveTools : ReserveTools
    }

    return service;

    // @brief List all the tools by task ID
    //
    // @param taskID is the id of the task related to the tools
    // @return promise to the HTTP request, caller must handle the promise
    function FindToolsByTaskID(taskID) {

      var request = $http({
                            method: 'GET',
                            url: __env.apiUrl + __env.baseUrl + '/' + taskID + '/tools',
                          });

      // return (request.then(HandleSuccess, HandleError));
      return request;
    };

    // @brief Authenticate the user with the external booking system
    //
    // @param cred is the credentials of the user
    // @return promise to the HTTP request, caller must handle the promise
    function Authenticate(cred) {
      var authdata = {
        "username": cred[0],
        "password": cred[1]
      }
      var authenticate = $http({
                            method: 'POST',
                            url: __env.bookedUrl + '/booked-mod/Web/Services/Authentication/Authenticate',
                            data: angular.toJson(authdata),
                            headers: {
                              'Content-Type' : 'application/json'
                            }
                          });

      return authenticate;
    }

    // @brief Call the API in the external booking system to check availability of the tools. The call is made for each resource based on resource id.
    //
    // @param cred is the credentials of the user
    // @param tools is an array of tool objects
    // @return promise to the HTTP request, caller must handle the promise
    function CheckToolsAvailability(cred, tools) {

      console.log('check tools : ' + cred);
      let check = tools.map(tool => {
        // console.log(tool.Booked_rid);

        return $http({
          method: 'GET',
          url: __env.bookedUrl + '/booked-mod/Web/Services/Reservations/?resourceId=' + tool.Booked_rid,
          headers: {
            'X-Booked-SessionToken' : cred[0],
            'X-Booked-UserId' : cred[1]
          }
        });
      });

      // $q.all(check).then(values =>{
      //   console.log('check > ' + values);
      // });

      return $q.all(check);
    }

    // @brief Call the API in the external booking system to reserve a tool or multiple tool
    //
    // @param cred is the credentials of the user
    // @param tools is an array of tool objects
    // @return promise to the HTTP request, caller must handle the promise
    function ReserveTools(cred, tools) {

      var resourceIds = tools.map(tool => {
        console.log('tools : ' + tool['rid']);
        return tool['rid'];
      });

      var startDate = new Date();
      var endDate = new Date();
      endDate.setHours(18, 0, 0);
      var resources = resourceIds.splice(1,resourceIds.length)

      var send =
      {
      	"endDateTime": $filter('date')(endDate, 'yyyy-MM-dd HH:mm:ss', '+0800'),
      	"resourceId": resourceIds[0],
      	"resources": resources,
      	"startDateTime": $filter('date')(startDate, 'yyyy-MM-dd HH:mm:ss', '+0800'),
      	"userId": cred[1],
      	"title": ""
      }

      console.log(send);

      var request = $http({
        method: 'POST',
        url: __env.bookedUrl + '/booked-mod/Web/Services/Reservations/',
        data: angular.toJson(send),
        headers: {
          'X-Booked-SessionToken' : cred[0],
          'X-Booked-UserId' : cred[1]
        }
      });

      return (request.then(HandleSuccess, HandleError));
    }

    function HandleSuccess(response) {
      // Transform the successful response, unwrapping the application data
      // from the API response payload.
      return (response.data);
    };

    function HandleError(response) {
      // The API response from the server should be returned in a
      // nomralized format. However, if the request was not handled by the
      // server (or what not handles properly - ex. server error), then we
      // may have to normalize it on our end, as best we can.
      if (
          ! angular.isObject( response.data ) ||
          ! response.data.message
          ) {
          return( $q.reject( "An unknown error occurred." ) );
      }

      // Otherwise, use expected error message.
      return( $q.reject( response.data.message ) );
    };

  }

})();
