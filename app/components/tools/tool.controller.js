/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a controller to handle the tool view
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('ToolController', ToolController);

    ToolController.$inject = ['$scope', 'AuthenticationService', /*'$uibModalInstance',*/ '$sce', 'ToolService', /*'taskid',*/ 'Base64'];

    function ToolController($scope, AuthenticationService, /*$uibModalInstance,*/ $sce, ToolService, /*taskid,*/ Base64) {
      var vm = this;

      vm.render = true;
      vm.selectedID = 0;
      vm.toolList = null;

      vm.cookie = AuthenticationService.GetCredentials();
      var cred = Base64.decode(vm.cookie.currentUser['authdata']).split(':');
      console.log(cred)
      vm.email = cred[0];
      vm.password = cred[1];

      vm.action = $sce.trustAsResourceUrl(__env.bookedUrl + '/booked-mod/Web/index.php');
      vm.url = __env.bookedUrl + '/booked-mod/Web/reservation.php?';
      var temp = [];

      // @brief List all the tools for a particular task. It will also retrieve the tools availability from the external booking system.
      //
      // @param id is the id of the task related to the tools
      // @return void
      vm.FindTools = function(id) {
      // vm.FindTools = function() {

        console.log(id);

        let authdata = [];
        let temp = [];

        // Here we have multiple steps to find the list of tools.
        // We need to retrieve list from Smart Mobility database and then check the availability in booking system using its API.
        // This process will connect Smart Mobility to booking system which could hosted within HaoFu or externally.
        // Authentication is required first before we call the API.
        // Alternative way: You may choose to call ToolService.FindToolsByTaskID() and store it temporarily then authenticate and call the API
        ToolService.Authenticate(cred).then(info => {
          authdata[0] = info.data.sessionToken;
          authdata[1] = info.data.userId;

          // retrieve the list from Smart Mobility database and return to next then() below
          return ToolService.FindToolsByTaskID(id);
        })
        .then(result => {
          temp = result.data;
          console.log(temp);

          // Check tools availability from booking system.
          // Pass the list of tools retrieved from Smart Mobility database.
          return ToolService.CheckToolsAvailability(authdata, result.data);
        })
        .then(final => {
          console.log('result: ' + final);

          if(final == null) return;

          let rsc = [];

          // prepare the data for display in the views
          for(var i = 0; i < final.length; i++) {
            var lastRes = final[i].data.reservations.length;
            console.log('lastRes + ' + lastRes);
            var tool = {
              rid: temp[i]['Booked_rid'],
              name: temp[i]['Description'],
              partNo: (temp[i]['Part_Number'] == null) ? "-" : temp[i]['Part_Number'],
              nextBook: (lastRes > 0) ? final[i].data.reservations[lastRes-1]['startDate'] : 'AVAILABLE'
            }
            rsc.push(tool);
          }

          vm.toolList = rsc;

          console.log('toolList > ' + vm.toolList);

          // vm.url is the address of reservation page in the booking system.
          // We need to pass rid and aid as parameters for multiple resource booking at the same time.
          vm.url += 'rid=' + vm.toolList[0]['rid'];

          let add = []
          for(let x=1; x<vm.toolList.length; x++) {
            add[x-1] = vm.toolList[x]['rid'];
          }

          if(add.length > 0)
            vm.url += '&aid=' + JSON.stringify(add);

          $sce.trustAsResourceUrl(vm.url);
          // console.log(vm.url);
        })
        .catch(error => {
          // Please do proper error handling
        });
      }

      // @brief This function handles the manual tool booking. It fills up the values for the email and password into the forms.
      //
      // @param id is the id of the task related to the tools. It is used to retrieve the right form element in the view.
      // @return void
      vm.BookToolManual = function(id) {
        var element = document.getElementById('login-' + id);
        element.email.value = vm.email;
        element.password.value = vm.password;
        console.log("BookToolManual > " + element);
      }

      // @brief This function handles the tool booking via API
      //
      // @param taskid is the id of the task related to the tools
      // @param tools is the array list of tools
      // @return void
      vm.BookTool = function(taskid, tools) {

        // console.log(tools);

        let authdata = [];

        ToolService.Authenticate(cred).then(info => {
          authdata[0] = info.data.sessionToken;
          authdata[1] = info.data.userId;

          // console.log('info : ' + info);

          return ToolService.ReserveTools(authdata, tools);
        })
        .then(result => {
          // console.log('result : ' + result);
          vm.FindTools(taskid);
        });
      }

      // @brief Close the modal window
      //
      // @return void
      vm.Close = function () {
        $uibModalInstance.close();
      }

      //vm.InitialiseLocation();
      // vm.FindTools(taskid);
    }

})();
