/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a service to connect to Jobcard API
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .factory('JobcardService', JobcardService);

  JobcardService.$inject = ['$http', '$q', '__env'];

  function JobcardService($http, $q, __env) {

    var service = {
      ListByUser : ListByUser,
      ListAll: ListAll,
      UpdateByID : UpdateByID,
      // GetStatusByID: GetStatusByID
    }

    return service;

    // @brief List all the jobcard assigned to particular user ID
    //
    // @param userID is the id of the user log on to the application
    // @return promise to the HTTP request, caller must handle the promise
    function ListByUser(userID) {

      var request = $http({
                            method: 'GET',
                            url: __env.apiUrl + __env.baseUrl + '/' + userID + '/jobcards',
                          });

      return (request.then(HandleSuccess, HandleError));
    };

    // @brief List all the jobcard
    //
    // @return promise to the HTTP request, caller must handle the promise
    function ListAll() {
      var request = $http({
                            method: 'GET',
                            url: __env.apiUrl + __env.baseUrl + '/jobcards',
                          });

      return (request.then(HandleSuccess, HandleError));
    };

    // @brief Update jobcard by its ID
    //
    // @param jobcard is the jobcard data to update
    // @param newStatus is the status you want to update to
    // @return promise to the HTTP request, caller must handle the promise
    function UpdateByID(jobcard, newStatus) {

      var status = jobcard['status'];

      if(status === 1) return;
      // else if(status === 0) status = 2; // submit report to supervisor
      // else if(status === 2) status = 1; // supervisor validate the jobcard

      console.log('newStatus : ' + newStatus);

      jobcard['status'] = newStatus;
      var request = $http({
                            method: 'PUT',
                            url: __env.apiUrl + __env.baseUrl + '/mmob/jobcard/' + jobcard['id'],
                            data: angular.toJson(jobcard),
                            headers: {
                              'Content-Type' : 'application/json'
                            }
                          });

      return (request.then(HandleSuccess, HandleError));
    };

    // Get jobcard status by jobcardID
    // function GetStatusByID(jobcardID) {
    //
    //     var request = $http({
    //                           method: 'GET',
    //                           url: __env.apiUrl + __env.baseUrl + '/mmob/jobcard/status/' + jobcardID
    //                         });
    //
    //     return (request.then(HandleSuccess, HandleError));
    // }


    function HandleSuccess(response) {
      // Transform the successful response, unwrapping the application data
      // from the API response payload.
      return (response.data);
    };

    function HandleError(response) {
      // The API response from the server should be returned in a
      // nomralized format. However, if the request was not handled by the
      // server (or what not handles properly - ex. server error), then we
      // may have to normalize it on our end, as best we can.
      if (
          ! angular.isObject( response.data ) ||
          ! response.data.message
          ) {
          return( $q.reject( "An unknown error occurred." ) );
      }

      // Otherwise, use expected error message.
      return( $q.reject( response.data.message ) );
    };

  }

})();
