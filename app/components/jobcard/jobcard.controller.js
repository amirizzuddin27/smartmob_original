/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 *
 * @section DESCRIPTION
 *
 * This is a controller to handle the jobcard view
 *
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .controller('JobcardController', JobcardController);

  JobcardController.$inject = ['$scope', '$state', 'JobcardService', 'AuthenticationService', 'Notification', '$filter', 'SocketService'];

  function JobcardController($scope, $state, JobcardService, AuthenticationService, Notification, $filter, SocketService) {

    var vm = this;

    vm.cookie = AuthenticationService.GetCredentials();
    vm.today = new Date();

    // @brief Find and list jobcards for a particular user
    //
    // @param userID is the id of the user
    // @return void
    vm.ListByUser = function (userID) {
      console.log(userID);

      JobcardService.ListByUser(userID).then(function (data) {
        vm.listOfJC = data;
        // vm.totalPage = Math.ceil(vm.listOfJC.length / 3);
      });
    };

    // @brief Find and list all jobcards
    //
    // @return void
    vm.ListAll = function () {
      JobcardService.ListAll().then(function (data) {
        vm.listOfJC = data;
        // vm.totalPage = Math.ceil(vm.listOfJC.length / 3);
      })
    }

    SocketService.On('jobcard:update', function(data){
      console.log('jobcard:update');
      if(vm.cookie.currentUser['role'] == 1) {
        vm.ListAll();
        console.log('data : ' + data);
      } else {
        vm.ListByUser(vm.cookie.currentUser['email']);

      }
    });

    SocketService.On('task:update', function(data){
      console.log('task:update');
      if(vm.cookie.currentUser['role'] == 1) {
        vm.ListAll();
      } else {
        vm.ListByUser(vm.cookie.currentUser['email']);
      }
    });

    // @brief Update the status of jobcard
    //
    // @return void
    vm.UpdateStatusByID = function (index, newStatus) {
      var jobcard = vm.listOfJC[index];
      JobcardService.UpdateByID(jobcard, newStatus).then(function (data) {
        // vm.listOfJC[index]['status'] = data.status;
        vm.Initialize();
      });
    };

    // @brief Handles 'View Task' button
    //
    // @return void
    vm.DisplayTasks = function(jobcard) {
      $state.go('amm-worker', {jobcard: jobcard});
    };

    // @brief Handles 'View Finding' button
    //
    // @return void
    vm.PreviewReport = function(jobcardID, completed) {
      console.log('completed: ' + vm.cookie.currentUser['role']);
      var page = 'report';
      if(vm.cookie.currentUser['role'] == 1) {
        page = 'report-supervisor';
      }
      console.log('page : ' + page);
      $state.go(page, {jobcardID: jobcardID, completed: completed});
    }
    //
    // vm.PreviewReportSup = function(jobcardID, completed) {
    //   console.log(completed);
    //   $state.go('report-supervisor', {jobcardID: jobcardID, completed: completed});
    // }

    $scope.$on('$destroy', function (event) {
      SocketService.Stop();
      console.log('call destroy');
    });

    // @brief Initialize the jobcard controller
    //
    // @return void
    vm.Initialize = function() {
      //vm.ListByUser(UserService.GetUsername());
      if(vm.cookie.currentUser['role'] == 1) {
        vm.ListAll();
      } else {
        vm.ListByUser(vm.cookie.currentUser['email']);
      }
    }

    vm.Initialize();
  }

})();
