/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 */
(function() {
  'use strict';

  angular
    .module('mmobApp')
    .config(config);

    config.$inject=['$stateProvider', '$urlRouterProvider', '$compileProvider', 'NotificationProvider'];

    function config($stateProvider, $urlRouterProvider, $compileProvider, NotificationProvider) {

      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https|ftp|mailto|smarteredoc|chrome-extension):/);
      $urlRouterProvider.when('/dashboard', '/dashboard/jobcard');
      $urlRouterProvider.when('/dashboard-supervisor', '/dashboard-supervisor/jobcard-supervisor');
      // $urlRouterProvider.when('/report', '/report/report-worker');
      $urlRouterProvider.otherwise('/login');

      $stateProvider
        .state('login', {
          url: '/login',
          templateUrl: 'components/login/login.view.html'
        })

        // Worker
        .state('dashboard', {
          url: '/dashboard',
          templateUrl: 'components/dashboard/dashboard.view.html'
        })
        .state('jobcard', {
          url: '/jobcard',
          parent: 'dashboard',
          templateUrl: 'components/jobcard/jobcard.view.html'
        })
        .state('amm-worker', {
          url: '/task',
          parent: 'dashboard',
          templateUrl: 'components/task/task.view.html',
          params: {
            jobcard: null
          }
        })
        .state('report', {
          url: '/report',
          parent: 'dashboard',
          templateUrl: 'components/report/report.view.html',
          params: {
            jobcardID: null,
            completed: null
          }
        })

        // Supervisor
        .state('dashboard-supervisor', {
          url: '/dashboard-supervisor',
          templateUrl: 'components/dashboard/dashboard.supervisor.view.html'
        })
        .state('jobcard-supervisor', {
          url: '/jobcard-supervisor',
          parent: 'dashboard-supervisor',
          templateUrl: 'components/jobcard/jobcard.supervisor.view.html'
        })
        .state('report-supervisor', {
          url: '/report-supervisor',
          parent: 'dashboard-supervisor',
          templateUrl: 'components/report/report.supervisor.view.html',
          params: {
            jobcardID: null,
            completed: null
          }
        })

        // .state('amm-supervisor', {
        //   url: '/amm/:jobcardId',
        //   parent: 'dashboard-supervisor',
        //   templateUrl: 'app/dashboard-supervisor/amm/amm.view.html'
        // })
        // .state('report-supervisor', {
        //   url: '/report/',
        //   parent: 'dashboard-supervisor',
        //   templateUrl: 'app/dashboard-supervisor/report/report.view.html',
        //   params: {
        //     jobcardID: null,
        //     completed: null
        //   }
        // })

        NotificationProvider.setOptions({
          delay: 1500,
          startTop: 20,
          startRight: 180,
          verticalSpacing: 20,
          horizontalSpacing: 20,
          positionX: 'right',
          positionY: 'bottom',
          templateUrl: 'shared/notification/notification.view.html'
        });
    };

})();
