var log = function()
{
    var oldConsoleLog = null;
    var DEBUG = false;
    var pub = {};

    pub.enable = function(enable) {
      if(enable) {
        if(oldConsoleLog == null)
            return;

        window['console']['log'] = oldConsoleLog;
      } else {
        oldConsoleLog = console.log;
        window['console']['log'] = function() {};
      }
    }  ;

    return pub;
}();
