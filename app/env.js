/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 */

(function (window) {
  window.__env = window.__env || {};

  // location of the back-end server
  window.__env.bookedUrl = 'http://192.168.100.217';
  window.__env.apiUrl = ' http://api.smartmob.haofu.airbusdigital.com:443';
  window.__env.baseUrl = '/api/smartmob';

  // hardcoded value as we do not have aircraft registration table yet
  window.__env.acReg = '9V-STP';

  // true - displays the console.log, false - do not display
  window.__env.enableDebug = true;
}(this));
