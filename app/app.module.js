/* Copyright (C) Airbus Singapore Pte Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hangar of The Future Team
 */

(function() {
  'use strict';

  var env = {};

  // Import variables if present (from env.js)
  if(window){
    Object.assign(env, window.__env);
  }

  // Setting path for socket.io dynamically
  // var socketScript = document.createElement("script");
  // socketScript.type = "text/javascript";
  // socketScript.src = env.apiUrl + '/socket.io/socket.io.js';
  // document.getElementsByTagName("body")[0].appendChild(socketScript);

  // Define the angular app
  angular
  .module('mmobApp', ['ui.router', 'ui.bootstrap', 'webcam', 'ngCookies', 'ngAnimate', 'ui-notification'])
  .constant('__env', env);

  // @brief Define the filter to display '--' if the date is zero or empty
  //
  // @param dateString is the date value
  // @param format is the format to display the date
  angular
  .module('mmobApp')
  .filter('hideIfEmpty', ['$filter', function($filter) {
    return function (dateString, format) {
      // console.log(dateString);
      if(dateString === '0000-00-00 00:00:00') {
          return "--";
      } else {
          return $filter('date')(dateString, format.toString());
      }
    };
  }]);

  // @brief Define the filter to hide and unhide tasks depending on status
  //
  // @param tasks is the list of tasks to display based on list of statuses in the second parameter
  // @param status is list of statuses selected by user
  angular
  .module('mmobApp')
  .filter('taskFilter', ['$filter', function ($filter) {
      return function (tasks, status) {
          var filtered = [];

          angular.forEach(tasks, function (value, key) {
              if (status.indexOf(value["status"]) != -1) {
                  filtered.push(value);
              }
          }, filtered);

          return filtered;
      };
  }]);

  // true - displays the console.log, false - do not display
  log.enable(__env.enableDebug);

})();
